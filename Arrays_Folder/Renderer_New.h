#ifndef BIM_ENGINE_RENDERER_H
#define BIM_ENGINE_RENDERER_H

#include "GLEW\glew.h"
#include "glm\glm.hpp"
#include <vector>
#include "ShaderProgram.h"
#include "GeometryNode.h"
#include "CollidableNode.h"
#include "LightNode.h"

class Renderer
{
public:
	// Empty

protected:

	float											current_theta_cannon_1;


	float											sum_x, sum_y;
	int												m_screen_width, m_screen_height;
	int												last_translation_dt;
	bool											door_open;
	float											translation_value;
	glm::mat4										wall_1_translation;
	glm::mat4										wall_2_translation;
	glm::mat4										m_world_matrix;
	glm::mat4										m_view_matrix;
	glm::mat4										m_projection_matrix;
	glm::vec3										m_camera_position;
	glm::vec3										m_camera_target_position;
	glm::vec3										m_camera_up_vector;
	glm::vec2										m_camera_movement;
	glm::vec2										m_camera_look_angle_destination;
	
	float m_continous_time;

	// Protected Functions
	bool InitShaders();
	bool InitGeometricMeshes();
	bool InitCommonItems();
	bool InitLights();
	bool InitIntermediateBuffers();
	void BuildWorld();
	void InitCamera();
	void RenderGeometry();
	void RenderDeferredShading();
	void RenderStaticGeometry();
	void RenderCollidableGeometry();
	void RenderShadowMaps();
	void RenderPostProcess();

	//FIRST_PART
	enum GEOMETRY_OBJECTS {
		CANNON_GUNNER,
		CANNON_1,
		CANNON_2,
		CANNON_3,
		CANNON_4,
		CANNON_5,
		CANNON_6,
		CANNON_MOUNT_1,
		CANNON_MOUNT_2,
		CANNON_MOUNT_3,
		CANNON_MOUNT_4,
		IRIS_1,
		IRIS_2,
		IRIS_3,
		IRIS_4
		IRIS_5,
		IRIS_6,
		IRIS_7,
		PIPE_1,
		PIPE_2,
		PIPE_3,
		PIPE_4,
		PIPE_5,
		PIPE_6,
		PIPE_7,
		PIPE_8,
		PIPE_9,
		PIPE_10,
		PIPE_11,
		SIZE_ALL_GEO
	};
	
	enum COLLIDABLES_OBJECTS {
		BEAM_1 = 0,
		BEAM_2,
		BEAM_3,
		CORRIDOR_FORK_1,
		CORRIDOR_FORK_2,
		CORRIDOR_RIGHT_1,
		CORRIDOR_RIGHT_2,
		CORRIDOR_RIGHT_3,
		CORRIDOR_STRAIGHT_1,
		CORRIDOR_STRAIGHT_2,
		CORRIDOR_STRAIGHT_3,
		CORRIDOR_STRAIGHT_4,
		CORRIDOR_STRAIGHT_5,
		CORRIDOR_STRAIGHT_6,
		WALL_1,
		WALL_2,
		WALL_3,
		WALL_4,
		WALL_5,
		WALL_6,
		WALL_7,
		SIZE_ALL_COLL
	};

	//SECOND_PART
	/*enum GEOMETRY_OBJECTS {
		CANNON_GUNNER = 0,
		CANNON_7,
		CANNON_8,
		CANNON_MOUNT_5,
		IRIS_8,
		IRIS_9,
		IRIS_10,
		PIPE_12,
		PIPE_13,
		PIPE_14,
		PIPE_15,
		PIPE_16,
		PIPE_17,
		PIPE_18,
		PIPE_19,
		SIZE_ALL
	};

	enum COLLIDABLES_OBJECTS {
		BEAM_4 = 0,
		BEAM_5,
		CORRIDOR_FORK_3,
		CORRIDOR_FORK_4,
		CORRIDOR_RIGHT_4,
		CORRIDOR_RIGHT_5,
		CORRIDOR_RIGHT_6,
		CORRIDOR_STRAIGHT_7,
		CORRIDOR_STRAIGHT_8,
		CORRIDOR_STRAIGHT_9,
		CORRIDOR_STRAIGHT_10,
		WALL_8,
		WALL_9,
		WALL_10,
		WALL_11,
		WALL_12,
		WALL_13,
		WALL_14,
		WALL_15,
		SIZE_ALL
	};*/

	//ALL_PARTS
	/*enum GEOMETRY_OBJECTS {
		CANNON_GUNNER = 0,
		CANNON_1,
		CANNON_2,
		CANNON_3,
		CANNON_4,
		CANNON_5,
		CANNON_6,
		CANNON_7,
		CANNON_8,
		CANNON_MOUNT_1,
		CANNON_MOUNT_2,
		CANNON_MOUNT_3,
		CANNON_MOUNT_4,
		CANNON_MOUNT_5,
		IRIS_1,
		IRIS_2,
		IRIS_3,
		IRIS_4,
		IRIS_5,
		IRIS_6,
		IRIS_7,
		IRIS_8,
		IRIS_9,
		IRIS_10,
		PIPE_1,
		PIPE_2,
		PIPE_3,
		PIPE_4,
		PIPE_5,
		PIPE_6,
		PIPE_7,
		PIPE_8,
		PIPE_9,
		PIPE_10,
		PIPE_11,
		PIPE_12,
		PIPE_13,
		PIPE_14,
		PIPE_15,
		PIPE_16,
		PIPE_17,
		PIPE_18,
		PIPE_19,
		SIZE_ALL
	}
	
	enum COLLIDABLES_OBJECTS {
		BEAM_1 = 0,
		BEAM_2,
		BEAM_3,
		BEAM_4,
		BEAM_5,
		CORRIDOR_FORK_1,
		CORRIDOR_FORK_2,
		CORRIDOR_FORK_3,
		CORRIDOR_FORK_4,
		CORRIDOR_RIGHT_1,
		CORRIDOR_RIGHT_2,
		CORRIDOR_RIGHT_3,
		CORRIDOR_RIGHT_4,
		CORRIDOR_RIGHT_5,
		CORRIDOR_RIGHT_6,
		CORRIDOR_STRAIGHT_1,
		CORRIDOR_STRAIGHT_2,
		CORRIDOR_STRAIGHT_3,
		CORRIDOR_STRAIGHT_4,
		CORRIDOR_STRAIGHT_5,
		CORRIDOR_STRAIGHT_6,
		CORRIDOR_STRAIGHT_7,
		CORRIDOR_STRAIGHT_8,
		CORRIDOR_STRAIGHT_9,
		CORRIDOR_STRAIGHT_10,
		WALL_1,
		WALL_2,
		WALL_3,
		WALL_4,
		WALL_5,
		WALL_6,
		WALL_7,
		WALL_8,
		WALL_9,
		WALL_10,
		WALL_11,
		WALL_12,
		WALL_13,
		WALL_14,
		WALL_15,
		SIZE_ALL
	};*/

	std::vector<GeometryNode*> m_nodes;
	std::vector<CollidableNode*> m_collidables_nodes;

	LightNode									m_light;
	ShaderProgram								m_geometry_program;
	ShaderProgram								m_deferred_program;
	ShaderProgram								m_post_program;
	ShaderProgram								m_spot_light_shadow_map_program;

	GLuint m_fbo;
	GLuint m_vao_fbo;
	GLuint m_vbo_fbo_vertices;

	GLuint m_fbo_texture;

	GLuint m_fbo_depth_texture;
	GLuint m_fbo_pos_texture;
	GLuint m_fbo_normal_texture;
	GLuint m_fbo_albedo_texture;
	GLuint m_fbo_mask_texture;

public:

	Renderer();
	~Renderer();
	bool										Init(int SCREEN_WIDTH, int SCREEN_HEIGHT);
	void										Update(float dt, int counter, bool shooting_pose, bool shooting_enable, bool shooting_gunner);
	bool										ResizeBuffers(int SCREEN_WIDTH, int SCREEN_HEIGHT);
	void										UpdateGeometry(float dt, int counter);
	void										UpdateCamera(float dt, bool shooting_pose, bool shooting_enable, bool shooting_gunner);
	void										UpdateCollidableGeometryFromCamera();
	bool										ReloadShaders();
	void										Render();

	void										CameraMoveForward(bool enable);
	void										CameraMoveBackWard(bool enable);
	void										CameraMoveLeft(bool enable);
	void										CameraMoveRight(bool enable);
	void										CameraLook(glm::vec2 lookDir);
};

#endif
