
public class Translation{

    private Coordinates coords;

    public Translation(float x, float y, float z){
        coords = new Coordinates(x, y, z);
    }

    public Coordinates getCoords() {
        return coords;
    }

    public void setCoords(Coordinates coords) {
        this.coords = coords;
    }

}
