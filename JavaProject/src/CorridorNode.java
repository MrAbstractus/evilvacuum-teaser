import java.util.ArrayList;

public class CorridorNode {

    private Coordinates coords;
    private int enum_code;
    private int corridor_index;
    private String name_id;
    private int counter_beam;
    private int counter_cannon;
    private int counter_cannon_mount;
    private int counter_iris;
    private int counter_pipe;
    private int counter_wall;
    private final ArrayList<ClassBeam> beam_list;
    private final ArrayList<ClassCannon> cannon_list;
    private final ArrayList<ClassCannonMount> cannon_mount_list;
    private final ArrayList<ClassIris> iris_list;
    private final ArrayList<ClassPipe> pipe_list;
    private final ArrayList<ClassWall> wall_list;
    private final ArrayList<CorridorNode> next_list;
    private final int[] arraymaxobjects;

    public CorridorNode() {
        beam_list = new ArrayList<ClassBeam>();
        cannon_list = new ArrayList<ClassCannon>();
        cannon_mount_list = new ArrayList<ClassCannonMount>();
        iris_list = new ArrayList<ClassIris>();
        pipe_list = new ArrayList<ClassPipe>();
        wall_list = new ArrayList<ClassWall>();
        next_list = new ArrayList<CorridorNode>(2);
        arraymaxobjects = new int[6];
    }

    public CorridorNode(float x, float y, float z, int enum_code , int corridor_index, int counter_beam, int counter_cannon, int counter_cannon_mount, int counter_iris,
                                                                                                        int counter_pipe, int counter_wall) {
        this();
        this.enum_code = enum_code;
        this.corridor_index = corridor_index;
        this.counter_beam = counter_beam;
        this.counter_cannon = counter_cannon;
        this.counter_cannon_mount = counter_cannon_mount;
        this.counter_iris = counter_iris;
        this.counter_pipe = counter_pipe;
        this.counter_wall = counter_wall;
        arraymaxobjects[0] = counter_beam;
        arraymaxobjects[1] = counter_cannon;
        arraymaxobjects[2] = counter_cannon_mount;
        arraymaxobjects[3] = counter_iris;
        arraymaxobjects[4] = counter_pipe;
        arraymaxobjects[5] = counter_wall;
        setNameId();
    }

    public void setEnumCode(int enum_code) {
        this.enum_code = enum_code;
        setNameId();
    }

    public int getEnumCode() {
        return enum_code;
    }

    public void setCorridorIndex(int corridor_index) {
        this.corridor_index = corridor_index;
        setNameId();
    }

    public int getCorridorIndex() {
        return corridor_index;
    }

    private void setNameId() {
        if (enum_code == 1) {
            name_id = ("CORRIDOR_STRAIGHT_").concat(Integer.toString(corridor_index));
        } else if (enum_code == 2) {
            name_id = ("CORRIDOR_RIGHT_").concat(Integer.toString(corridor_index));
        } else if (enum_code == 3) {
            name_id = ("CORRIDOR_FORK_").concat(Integer.toString(corridor_index));
        }
    }

    public String getNameId() {
        return name_id;
    }

    public ArrayList<CorridorNode> getNextList() {
        return next_list;
    }

    public void clearArrayMaxObjects() {
        for (int i : arraymaxobjects) {
            i = 0;
        }
    }

    public void setMaxObject(int index, int number) {
        if (index >= 0 && index < arraymaxobjects.length) {
            arraymaxobjects[index] = number;
        }
    }

    public int getMaxObject(int index) {
        if (index >= 0 && index < arraymaxobjects.length) {
            return arraymaxobjects[index];
        }
        return 0;
    }

    public int[] getArraymaxobjects() {
        return arraymaxobjects;
    }

    public void setCounterBeam(int counter_beam) {
        this.counter_beam = counter_beam;
        arraymaxobjects[0] = counter_beam;
    }

    public int getCounterBeam() {
        return counter_beam;
    }

    public void setCounterCannon(int counter_cannon) {
        this.counter_cannon = counter_cannon;
        arraymaxobjects[1] = counter_cannon;
    }

    public int getCounterCannon() {
        return counter_cannon;
    }

    public void setCounterCannonMount(int counter_cannon_mount) {
        this.counter_cannon_mount = counter_cannon_mount;
        arraymaxobjects[2] = counter_cannon_mount;
    }

    public int getCounterCannonMount() {
        return counter_cannon_mount;
    }

    public void setCounterIris(int counter_iris) {
        this.counter_iris = counter_iris;
        arraymaxobjects[3] = counter_iris;
    }

    public int getCounterIris() {
        return counter_iris;
    }

    public void setCounterPipe(int counter_pipe) {
        this.counter_pipe = counter_pipe;
        arraymaxobjects[4] = counter_pipe;
    }

    public int getCounterPipe() {
        return counter_pipe;
    }

    public void setCounterWall(int counter_wall) {
        this.counter_wall = counter_wall;
        arraymaxobjects[5] = counter_wall;
    }

    public int getCounterWall() {
        return counter_wall;
    }

    public void insertNextCorridor(CorridorNode next_corridor_node) {
        if (next_list.size() < 2) {
            next_list.add(next_corridor_node);
        }
    }

    public void printCorridorContext() {
        System.out.println("----------------");
        System.out.println("Enum Code: " + enum_code + ", Corridor Index: " + corridor_index + ", Name: " + name_id);
        System.out.println("Beam: " + counter_beam + ", Cannon: " + counter_cannon + ", Cannon Mount: " + counter_cannon_mount);
        System.out.println("Iris: " + counter_iris + ", Pipe: " +  counter_pipe + ", Wall: " + counter_wall);
        if (!next_list.isEmpty()) {
            for (CorridorNode node : next_list) {
                System.out.print(node.getNameId() + " ");
            }
            System.out.println();
        }
    }

    public void addBeam(ClassBeam beam){
        if(beam_list.size() < counter_beam){
            beam_list.add(beam);
        }
    }

    public void addCannon(ClassCannon cannon){
        if(cannon_list.size() < counter_cannon){
            cannon_list.add(cannon);
        }
    }

    public void addWall(ClassWall wall){
        if(wall_list.size() < counter_wall){
            wall_list.add(wall);
        }
    }

    public void addPipe(ClassPipe pipe){
        if(pipe_list.size() < counter_pipe){
            pipe_list.add(pipe);
        }
    }

    public void addCannonMount(ClassCannonMount cannonMount){
        if(cannon_mount_list.size() < counter_cannon_mount){
            cannon_mount_list.add(cannonMount);
        }
    }

    public void addIris(ClassIris iris){
        if(iris_list.size() < counter_iris){
            iris_list.add(iris);
        }
    }

}
