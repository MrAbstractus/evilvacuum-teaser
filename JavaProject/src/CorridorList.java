import java.util.*;

public class CorridorList {

    private CorridorNode head_corridor;
    private final ArrayList<CorridorNode> corridor_list;

    public CorridorList() {
        head_corridor = null;
        corridor_list = new ArrayList<CorridorNode>();
    }

    public CorridorList(CorridorNode head_corridor) {
        this();
        insertHead(head_corridor);
    }

    public void insertHead(CorridorNode head_corridor) {
        this.head_corridor = head_corridor;
        corridor_list.clear();
        corridor_list.add(head_corridor);
    }

    public void insertNextCorridor(CorridorNode previous_node, CorridorNode next_node) {
        for (CorridorNode node : corridor_list) {
            if (node.getEnumCode() == previous_node.getEnumCode() && node.getCorridorIndex() == previous_node.getCorridorIndex()) {
                node.insertNextCorridor(next_node);
                corridor_list.add(next_node);
                break;
            }
        }
    }

    public void insertUpsideDownCorridorFork(CorridorNode previous_node_1, CorridorNode previous_node_2, CorridorNode next_node) {
        int counter = 0;
        for (CorridorNode node : corridor_list) {
            if (node.getEnumCode() == previous_node_1.getEnumCode() && node.getCorridorIndex() == previous_node_1.getCorridorIndex()) {
                node.insertNextCorridor(next_node);
                counter++;
            } else if (node.getEnumCode() == previous_node_2.getEnumCode() && node.getCorridorIndex() == previous_node_2.getCorridorIndex()) {
                node.insertNextCorridor(next_node);
                counter++;
            }
            if (counter == 2) {
                corridor_list.add(next_node);
                break;
            }
        }
    }

    public int getSizeList() {
        return corridor_list.size();
    }

    public void calculateMaxObjects(int depth) {
        for (CorridorNode node : corridor_list) {
            calculateMax(node, 1, depth);
        }
    }

    public void calculateMax(CorridorNode node, int counter, int depth) {
        if (counter <= depth) {
            int[] array = new int[6];
            for (CorridorNode next_node : node.getNextList()) {
                array = next_node.getArraymaxobjects();
                if (array[0] < node.getCounterBeam() + next_node.getCounterBeam()) {
                    next_node.setMaxObject(0, node.getCounterBeam() + next_node.getCounterBeam());
                }
                if (array[1] < node.getCounterCannon() + next_node.getCounterCannon()) {
                    next_node.setMaxObject(1, node.getCounterCannon() + next_node.getCounterCannon());
                }
                if (array[2] < node.getCounterCannonMount() + next_node.getCounterCannonMount()) {
                    next_node.setMaxObject(2, node.getCounterCannonMount() + next_node.getCounterCannonMount());
                }
                if (array[3] < node.getCounterIris() + next_node.getCounterIris()) {
                    next_node.setMaxObject(3, node.getCounterIris() + next_node.getCounterIris());
                }
                if (array[4] < node.getCounterPipe() + next_node.getCounterPipe()) {
                    next_node.setMaxObject(4, node.getCounterPipe() + next_node.getCounterPipe());
                }
                if (array[5] < node.getCounterWall() + next_node.getCounterWall()) {
                    next_node.setMaxObject(5, node.getCounterWall() + next_node.getCounterWall());
                }
                calculateMax(next_node, ++counter, depth);
            }
        }
    }

    public int[] getMaxObjects() {
        int[] array = new int[6];
        for (CorridorNode node : corridor_list) {
            if (array[0] < node.getMaxObject(0)) {
                array[0] = node.getMaxObject(0);
            }
            if (array[1] < node.getMaxObject(1)) {
                array[1] = node.getMaxObject(1);
            }
            if (array[2] < node.getMaxObject(2)) {
                array[2] = node.getMaxObject(2);
            }
            if (array[3]< node.getMaxObject(3)) {
                array[3] = node.getMaxObject(3);
            }
            if (array[4] < node.getMaxObject(4)) {
                array[4] = node.getMaxObject(4);
            }
            if (array[5] < node.getMaxObject(5)) {
                array[5] = node.getMaxObject(5);
            }
        }
        return array;
    }

    public void printAllCorridors() {
        if (!corridor_list.isEmpty()) {
            for (CorridorNode node : corridor_list) {
                node.printCorridorContext();
            }
        }
    }
}
