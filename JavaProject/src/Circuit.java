
public class Circuit {

    public static void main(String[] args) {

        CorridorNode corridor_1 = new CorridorNode(0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 2);
        ClassWall wall1 = new ClassWall();
        wall1.addTranslation(new Translation(-5, 0, -20));
        corridor_1.addWall(wall1);
        ClassWall wall2 = new ClassWall();
        wall2.addTranslation(new Translation(5, 0, -20));
        corridor_1.addWall(wall2);
        ClassPipe pipe1 = new ClassPipe();
        pipe1.addTranslation(new Translation(0, 3.5F, 0));
        corridor_1.addPipe(pipe1);
        ClassPipe pipe2 = new ClassPipe();
        pipe2.addRotation(new Rotation("y", 90));
        pipe2.addTranslation(new Translation(10, 3.5F, -10));
        corridor_1.addPipe(pipe2);

        CorridorNode corridor_2 = new CorridorNode(0, 0, -20, 1, 1, 2, 1, 2, 1, 1, 1);
        ClassPipe pipe3 = new ClassPipe();
        pipe3.addRotation(new Rotation("y", 90));
        pipe3.addTranslation(new Translation(12, -3.5F, -30));
        corridor_2.addPipe(pipe3);
        ClassPipe pipe4 = new ClassPipe();
        pipe4.addTranslation(new Translation(2, -3.5F, -30));
        corridor_2.addPipe(pipe4);
        ClassPipe pipe5 = new ClassPipe();
        pipe5.addRotation(new Rotation("y", 90));
        pipe5.addTranslation(new Translation(2, -3.5F, -40));
        corridor_2.addPipe(pipe5);

        CorridorNode corridor_3 = new CorridorNode(0, 0, -40, 1, 2, 1, 2, 1, 1, 1, 3);
        CorridorNode corridor_4 = new CorridorNode(0, 0, -60, 2, 2, 1, 2, 1, 1, 2, 1);
        CorridorNode corridor_5 = new CorridorNode(0, 0, -80, 3, 1, 1, 1, 1, 1, 1, 1);
        CorridorNode corridor_6 = new CorridorNode(0, 0, -120, 1, 3, 1, 1, 1, 1, 1, 1);
        CorridorNode corridor_7 = new CorridorNode(0, 0, -120, 1, 4, 1, 1, 1, 1, 1, 1);
        CorridorNode corridor_8 = new CorridorNode(0, 0, -140, 3, 2, 1, 1, 1, 1, 1, 1);
        CorridorList circuit = new CorridorList(corridor_1);
        circuit.insertNextCorridor(corridor_1, corridor_2);
        circuit.insertNextCorridor(corridor_2, corridor_3);
        circuit.insertNextCorridor(corridor_3, corridor_4);
        circuit.insertNextCorridor(corridor_4, corridor_5);
        circuit.insertNextCorridor(corridor_5, corridor_6);
        circuit.insertNextCorridor(corridor_5, corridor_7);
        circuit.insertUpsideDownCorridorFork(corridor_6, corridor_7, corridor_8);
        System.out.println("Circuit size: " + circuit.getSizeList());
        circuit.printAllCorridors();
        circuit.calculateMaxObjects(2);
        int[] array = circuit.getMaxObjects();
        System.out.println("----------------");
        System.out.println("MAX: " + array[0] + " " + array[1] + " " + array[2] + " " + array[3] + " " + array[4] + " " + array[5]);
    }

}
