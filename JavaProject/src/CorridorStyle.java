
public enum CorridorStyle {

    STRAIGHT(1),
    RIGHT(2),
    FORK(3);

    int num;

    private CorridorStyle(int num){
        this.num = num;
    }

}
