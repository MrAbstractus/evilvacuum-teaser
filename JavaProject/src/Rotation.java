
public class Rotation{

    private float degrees;
    private String axis;

    public Rotation(String axis, float degrees){
        this.axis = axis;
        this.degrees = degrees;
    }

    public float getDegrees() {
        return degrees;
    }

    public void setDegrees(float degrees) {
        this.degrees = degrees;
    }

    public String getAxis() {
        return axis;
    }

    public void setAxis(String axis) {
        this.axis = axis;
    }
}
